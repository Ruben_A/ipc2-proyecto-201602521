﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaAgente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ComboxTecnico.Enabled = false;
        }

        protected void BotonCerrarSesion_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaginaPrincipal.aspx");
            this.Dispose();
        }

        protected void ComboBoxTipoInscripcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxTipoInscripcion.SelectedItem.Text.Equals("SitioTuristico"))
            {
                ComboxTecnico.Enabled=false;
            }//hay que habilitar en las opciones del DropDownListe en el diseno web la opcion AutoPostBack para que funcione el evento de seleccion

            else if (ComboBoxTipoInscripcion.SelectedItem.Text.Equals("EmpresaTuristica"))
            {
                ComboxTecnico.Enabled = true;
            }
        }
    }
}