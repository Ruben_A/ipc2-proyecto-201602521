﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaAdministrador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonCerrarSesionAdmon_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaginaPrincipal.aspx");
            this.Dispose();
        }
    }
}