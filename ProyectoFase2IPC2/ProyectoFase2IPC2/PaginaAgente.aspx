﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaAgente.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaAgente" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Agente-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoAgente">
    <form id="form1" runat="server">
        <div class="OtroH11">
    <h1  class="OtroH11">MODULO AGENTE
            </h1>
            <div class="OtroH11">   
                        <asp:Button ID="BotonCerrarSesion" runat="server" class="Botones3C" Text="Cerrar Sesion" OnClick="BotonCerrarSesion_Click" />

            </div>
            <h1  class="OtroH11">
                <asp:DropDownList ID="ComboBoxTipoInscripcion" runat="server" Height="28px" Width="203px" OnSelectedIndexChanged="ComboBoxTipoInscripcion_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem>Tipo Inscripcion</asp:ListItem>
                    <asp:ListItem>SitioTuristico</asp:ListItem>
                    <asp:ListItem>EmpresaTuristica</asp:ListItem>
                </asp:DropDownList>
&nbsp;<asp:DropDownList ID="ComboxSeleccionarCodigoUser" runat="server" Height="54px" Width="193px" style="margin-left: 0px">
                    <asp:ListItem>Seleccionar_Region</asp:ListItem>
                    <asp:ListItem>Norte</asp:ListItem>
                    <asp:ListItem>Sur</asp:ListItem>
                    <asp:ListItem>Este</asp:ListItem>
                    <asp:ListItem>Oeste</asp:ListItem>
                </asp:DropDownList>
&nbsp;
                &nbsp;<asp:DropDownList ID="ComboxTecnico" runat="server" Height="54px" Width="193px" style="margin-left: 0px">
                    <asp:ListItem>Seleccionar Tecnico</asp:ListItem>
                </asp:DropDownList>
&nbsp;
                <asp:Button ID="BuscarFormulariosParaInscripcion" runat="server" class="BotonesC" Text="OBTENER FORMULARIOS" />
            </h1>

            <br />

        </div>

        <div style="margin-left:200px">

       
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>

        </div>


    <div class="OtroH11">
 
        <br />
        <br />
        <br />
        

        
        <asp:Button ID="Cargar_Sitio_Empresa" runat="server" class="BotonesC" Text="CARGAR" />
        <br />
        <br />

        <br />

    </div>
       


    


    </form>
   
    

    

</body>

</html>
