﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Loguin.aspx.cs" Inherits="ProyectoFase2IPC2.Loguin" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>IniciarSesion</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body>
    <h1>BIENVENIDO</h1>
    <div class=" w3l-login-form">
        <h2>INICIAR SESION</h2>
        <form id="form1" runat="server">

            <div class=" w3l-form-group">
                <label>Nombre_De_Usuario:</label>
                <div class="group">
                   
                        
                   <i class="fas fa-user"></i>
                        <asp:TextBox ID="TextBoxNombreDeUser" runat="server" Width="431px"></asp:TextBox>
                
                </div>
            </div>
            <div class=" w3l-form-group">
                <label>Contraseña:</label>
                <div class="group">
                
                <i class="fas fa-unlock"> </i>
                    <asp:TextBox ID="TextBoxPasswordUser" TextMode="Password" runat="server" Width="393px"></asp:TextBox>
                </div>
            </div>
            <div class="forgot">
              
            </div>
          
            <asp:Button ID="Button1" class="BotonesC" runat="server" Text="Iniciar Sesion" Width="387px" OnClick="Button1_Click" />
        </form>
       
    </div>
    

</body>

</html>