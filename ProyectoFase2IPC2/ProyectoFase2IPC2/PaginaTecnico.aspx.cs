﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaTecnico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BotonVisitarEmpresas_Click(object sender, EventArgs e)
        {
            string _open = "window.open('PaginaVisitaTecnica.aspx', '_newtab');";//sirve para abrir en una nueva pestaña otra ventana o web form
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        protected void BotonEvaluarVisitaTecnica_Click(object sender, EventArgs e)
        {
            string _open = "window.open('PaginaEvaluarVisitaTecnica.aspx', '_newtab');";//sirve para abrir en una nueva pestaña otra ventana o web form
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        protected void ButtonCerrarSessionTec_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaginaPrincipal.aspx");
            this.Dispose();
        }
    }
}