﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaVisitante.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaVisitante" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Visitante-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoVisita" >
    <form id="form1" runat="server">
        <div>
    <h1  class="OtroH11">BIENVENIDO VISITANTE</h1>
            <h1  class="OtroH11">
&nbsp;<asp:DropDownList ID="ComboxSeleccionarEmpresa" runat="server" Height="54px" Width="193px" style="margin-left: 0px">
                    <asp:ListItem>Seleccionar Region :</asp:ListItem>
                    <asp:ListItem>Norte</asp:ListItem>
                    <asp:ListItem>Sur</asp:ListItem>
                    <asp:ListItem>Este</asp:ListItem>
                    <asp:ListItem>Oeste</asp:ListItem>
                </asp:DropDownList>
                 &nbsp;<asp:DropDownList ID="ComboxSeleccionarInfo" runat="server" Height="28px" Width="203px">
                    <asp:ListItem>Tipo Informacion :</asp:ListItem>
                    <asp:ListItem>SitioTuristico</asp:ListItem>
                    <asp:ListItem>EmpresaTuristica</asp:ListItem>
                </asp:DropDownList>
&nbsp;
                <asp:Button ID="BuscarRecorridoEvaluar" runat="server" class="BotonesC" Text="OBTENER INFORMACION" />
            &nbsp;&nbsp;&nbsp;&nbsp; </h1>
            
            
        

            <br />

        </div>
           


    <div  style="margin-left:200px">
 
        <br />
        <br />
        <br />
        

        
        

        
        <asp:GridView ID="GridView1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2">
            <Columns>
                <asp:CheckBoxField />
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;<br />
        <br />

        <br />

    </div>
   
    </form>
   
    

    

</body>

</html>


