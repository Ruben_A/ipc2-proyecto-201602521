﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaFormularioEmpresa.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaFormularioEmpresa" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulario-Empresa-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoFormulario">
    <form id="form1" runat="server">
        <div class="OtroH11">
    <h1  class="OtroH11">INSCRIPCION AL SITIO WEB
        
            </h1>
           
            
&nbsp;
                

            <br />

        </div>

        <div>

        </div>
       
        <div style="margin-left: 40px">


            <asp:Label ID="LabelDPI" runat="server" Text="Nombre* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<asp:TextBox ID="TextBox1" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label1" runat="server" Text="Direccion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label2" runat="server" Text="Region* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            <asp:DropDownList ID="DropDownList1" runat="server" Height="28px" Width="170px">
                <asp:ListItem>Seleccione_Region:</asp:ListItem>
                <asp:ListItem>Norte</asp:ListItem>
                <asp:ListItem>Sur</asp:ListItem>
                <asp:ListItem>Este</asp:ListItem>
                <asp:ListItem>Oeste</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
              <asp:Label ID="Label9" runat="server" Text="Tipo* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            <asp:DropDownList ID="DropDownList2" runat="server" Height="28px" Width="173px">
                <asp:ListItem>Seleccione_Tipo:</asp:ListItem>
                <asp:ListItem>Hotel</asp:ListItem>
                <asp:ListItem>Restaurante</asp:ListItem>
                <asp:ListItem>Museo</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
             <asp:Label ID="Label3" runat="server" Text="Fotografia* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            <asp:FileUpload ID="CargarFotos" runat="server" />
            &nbsp;<br />
            <br />
             <asp:Label ID="Label4" runat="server" Text="Telefono :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox5" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label5" runat="server" Text="Hora_De_Atencion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox6" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
             
            <br />
            <br />
                         <asp:Label ID="Label6" runat="server" Text="Hora_Fin_Atencion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox3" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>

            <br />
            <br />
                         <asp:Label ID="Label7" runat="server" Text="Tarifa :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox4" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
                           <asp:Label ID="Label8" runat="server" Text="Especialidad*  :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
            <asp:DropDownList ID="DropDownList3" runat="server" Height="33px" Width="176px">
                <asp:ListItem>Seleccione_Especialidad</asp:ListItem>
                <asp:ListItem>Pollo</asp:ListItem>
                <asp:ListItem>Pasta_Con_Ciruelas</asp:ListItem>
                <asp:ListItem>Sandia_Caramelizada</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
              <asp:Label ID="Label10" runat="server" Text="Servicios * :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            <asp:DropDownList ID="DropDownList4" runat="server" Height="32px" Width="175px">
                <asp:ListItem>Seleccione_Servicios:</asp:ListItem>
                <asp:ListItem>wifi</asp:ListItem>
                <asp:ListItem>cable</asp:ListItem>
                <asp:ListItem>bar</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
         

        </div>


    <div>
 
        <br />
<br />
        <div class="OtroH11">
        <asp:Button ID="BotonCargarFormulario" runat="server" class="BotonesC" Text="CARGAR FORMULARIO" />

        &nbsp;&nbsp; 
        &nbsp;&nbsp;
        &nbsp;<br />
        <br />

        <br />
            </div>
    </div>
       


    


    </form>
   
    

    

</body>

</html>
