
CREATE DATABASE ProyectoIPC2;
USE ProyectoIPC2;

---------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE catalogo_servicio (
    id_servicio_catalogo       INTEGER NOT NULL,
    nombre_servicio_catalogo   VARCHAR(200) NOT NULL
);

ALTER TABLE catalogo_servicio ADD CONSTRAINT catalogo_servicio_pk PRIMARY KEY ( id_servicio_catalogo );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE detalle_servicio (
    id_empresa   INTEGER NOT NULL,
    id_service   INTEGER NOT NULL
);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE empresa_turistica (
    id_empresa_turistic         INTEGER NOT NULL,
    tipo_empressa               INTEGER NOT NULL,
    nombre_empressa             VARCHAR(250) NOT NULL,
    direccion_empressa          VARCHAR(250) NOT NULL,
    telefono_empressa           VARCHAR(70) NOT NULL,
    email_empressa              VARCHAR(150) NOT NULL,
    region_empressa             INTEGER NOT NULL,
    horario_atencion            VARCHAR(15),
    horario_fin_atencion        VARCHAR(15),
    tarifa                      VARCHAR(20),
    especialidad                INTEGER,
    estado_inscripcion_empres   VARCHAR(250) NOT NULL,
    fecha_creacion              DATE NOT NULL
);

ALTER TABLE empresa_turistica ADD CONSTRAINT empresa_turistica_pk PRIMARY KEY ( id_empresa_turistic );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE especialidad (
    id_especialidad       INTEGER NOT NULL,
    nombre_especialidad   VARCHAR(200) NOT NULL
);

ALTER TABLE especialidad ADD CONSTRAINT especialidad_pk PRIMARY KEY ( id_especialidad );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE evaluacion_tecnica (
    id_evalucion_tecnic        INTEGER NOT NULL,
    numero_recibo_evaluacion   INTEGER NOT NULL,
    id_usuario_evaluacion1     INTEGER NOT NULL
);

ALTER TABLE evaluacion_tecnica ADD CONSTRAINT evaluacion_tecnica_pk PRIMARY KEY ( id_evalucion_tecnic );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE fotografia_sitio (
    titulo_foto_sitio   VARCHAR(250) NOT NULL,
    path                MEDIUMBLOB NOT NULL,
    id_sitio_foto       INTEGER NOT NULL
);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE fotografia_empresa (
    titulo_foto       VARCHAR(250) NOT NULL,
    path              MEDIUMBLOB NOT NULL,
    id_empresa_foto   INTEGER NOT NULL
);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE recibo_empresa (
    id_recibo            INTEGER NOT NULL,
    numero_correlativo   INTEGER NOT NULL,
    precio_pagar         NUMERIC(10,6) NOT NULL,
    id_empresa_recibo    INTEGER NOT NULL
);

ALTER TABLE recibo_empresa ADD CONSTRAINT recibo_empresa_pk PRIMARY KEY ( id_recibo );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE region (
    codigo_regionn   INTEGER NOT NULL,
    nombre_regionn   VARCHAR(200) NOT NULL
);

ALTER TABLE region ADD CONSTRAINT region_pk PRIMARY KEY ( codigo_regionn );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE sitio_turistico (
    id_sitio_turistic            INTEGER NOT NULL,
    nombre_sitio_turistic        VARCHAR(250) NOT NULL,
    descripcion_sitio_turistic   VARCHAR(250) NOT NULL,
    region_sitio_turistic        INTEGER NOT NULL,
    estado_inscripcion           VARCHAR(250) NOT NULL
);

ALTER TABLE sitio_turistico ADD CONSTRAINT sitio_turistico_pk PRIMARY KEY ( id_sitio_turistic );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE tipo (
    id_tipo       INTEGER NOT NULL,
    nombre_tipo   VARCHAR(200) NOT NULL
);

ALTER TABLE tipo ADD CONSTRAINT tipo_pk PRIMARY KEY ( id_tipo );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE usuario_administracion (
    id_usuario              INTEGER NOT NULL,
    nombre_usuario          VARCHAR(200) NOT NULL,
    telefono_usuario        VARCHAR(45) NOT NULL,
    email_usuario           VARCHAR(200) NOT NULL,
    nombre_usuario_loguin   VARCHAR(150) NOT NULL,
    contrasena_usuario      VARCHAR(150) NOT NULL,
    rol_usuario             VARCHAR(150) NOT NULL
);

ALTER TABLE usuario_administracion ADD CONSTRAINT usuario_administracion_pk PRIMARY KEY ( id_usuario );
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE visita_tecnica (
    id_visita_tecnic         INTEGER NOT NULL,
    nombre_recorrido         VARCHAR(100) NOT NULL,
    fecha_recorrido_visita   DATE NOT NULL,
    id_usuario_visita1       INTEGER NOT NULL,
    id_empresa_visita        INTEGER NOT NULL
);

ALTER TABLE visita_tecnica ADD CONSTRAINT visita_tecnica_pk PRIMARY KEY ( id_visita_tecnic );
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE detalle_servicio
    ADD CONSTRAINT detalle_servicio_catalogo_servicio_fk FOREIGN KEY ( id_service )
        REFERENCES catalogo_servicio ( id_servicio_catalogo );

ALTER TABLE detalle_servicio
    ADD CONSTRAINT detalle_servicio_empresa_turistica_fk FOREIGN KEY ( id_empresa )
        REFERENCES empresa_turistica ( id_empresa_turistic );

ALTER TABLE empresa_turistica
    ADD CONSTRAINT empresa_turistica_especialidad_fk FOREIGN KEY ( especialidad )
        REFERENCES especialidad ( id_especialidad );

ALTER TABLE empresa_turistica
    ADD CONSTRAINT empresa_turistica_region_fk FOREIGN KEY ( region_empressa )
        REFERENCES region ( codigo_regionn );

ALTER TABLE empresa_turistica
    ADD CONSTRAINT empresa_turistica_tipo_fk FOREIGN KEY ( tipo_empressa )
        REFERENCES tipo ( id_tipo );

--  ERROR: FK name length exceeds maximum allowed length(30) 
ALTER TABLE evaluacion_tecnica
    ADD CONSTRAINT evaluacion_tecnica_recibo_empresa_fk FOREIGN KEY ( numero_recibo_evaluacion )
        REFERENCES recibo_empresa ( id_recibo );


ALTER TABLE evaluacion_tecnica
    ADD CONSTRAINT evaluacion_tecnica_usuario_administracion_fk FOREIGN KEY ( id_usuario_evaluacion1 )
        REFERENCES usuario_administracion ( id_usuario );

ALTER TABLE fotografia_sitio
    ADD CONSTRAINT fotografia_sitio_sitio_turistico_fk FOREIGN KEY ( id_sitio_foto )
        REFERENCES sitio_turistico ( id_sitio_turistic );

ALTER TABLE fotografiaempresa
    ADD CONSTRAINT fotografiaempresa_empresa_turistica_fk FOREIGN KEY ( id_empresa_foto )
        REFERENCES empresa_turistica ( id_empresa_turistic );

ALTER TABLE recibo_empresa
    ADD CONSTRAINT recibo_empresa_empresa_turistica_fk FOREIGN KEY ( id_empresa_recibo )
        REFERENCES empresa_turistica ( id_empresa_turistic );

ALTER TABLE sitio_turistico
    ADD CONSTRAINT sitio_turistico_region_fk FOREIGN KEY ( region_sitio_turistic )
        REFERENCES region ( codigo_regionn );

ALTER TABLE visita_tecnica
    ADD CONSTRAINT visita_tecnica_empresa_turistica_fk FOREIGN KEY ( id_empresa_visita )
        REFERENCES empresa_turistica ( id_empresa_turistic );

ALTER TABLE visita_tecnica
    ADD CONSTRAINT visita_tecnica_usuario_administracion_fk FOREIGN KEY ( id_usuario_visita1 )
        REFERENCES usuario_administracion ( id_usuario );

