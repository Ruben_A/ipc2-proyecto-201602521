﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaFormularioIncripcion.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaFormularioIncripcion" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sitio-Formulario-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoFormulario">
    <form id="form1" runat="server">
        <div class="OtroH11">
    <h1  class="OtroH11">INSCRIPCION AL SITIO WEB</h1>
                

            <br />

        </div>

        <div>

        </div>
       
        <div style="margin-left: 40px">


            <asp:Label ID="LabelDPI" runat="server" Text="Nombre* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <asp:TextBox ID="TextBoxNombreSitio" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label1" runat="server" Text="Descripcion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxDescripcionSitio" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />

             <asp:Label ID="Label2" runat="server" Text="Region* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            
            <asp:DropDownList ID="ComboBoxRegionSitio" runat="server" Height="33px" Width="166px" AutoPostBack="True" OnSelectedIndexChanged="ComboBoxRegionSitio_SelectedIndexChanged">
                <asp:ListItem>Seleccione_Region:</asp:ListItem>
            </asp:DropDownList>
                                    </ContentTemplate>

                   <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboBoxRegionSitio"
                                EventName="SelectedIndexChanged" />
                                 </Triggers>
                 </asp:UpdatePanel>
            <br />
            <br />
             <asp:Label ID="Label3" runat="server" Text="Fotografia* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
            <asp:FileUpload ID="CargarFotos" AllowMultiple="true" runat="server" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="CargarFotosBaseDatos" runat="server" class="BotonesC" Text="CargarFotos" OnClick="CargarFotosBaseDatos_Click" />
            <br />
            <br />

        </div>


    <div>
 
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <div class="OtroH11">
        <asp:Button ID="BotonCargarFormulario" runat="server" class="BotonesC" Text="CARGAR FORMULARIO" OnClick="BotonCargarFormulario_Click" />

        &nbsp;&nbsp; 
        &nbsp;&nbsp;
        &nbsp;<br />
        <br />

        <br />
            </div>
    </div>
       


    


    </form>
   
    

    

</body>

</html>