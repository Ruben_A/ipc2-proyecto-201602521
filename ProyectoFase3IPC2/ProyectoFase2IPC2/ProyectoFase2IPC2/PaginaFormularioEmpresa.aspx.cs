﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaFormularioEmpresa : System.Web.UI.Page
    {
        public static char LlenarServicios;
        public static char LlenarEspecialidad;

        public string guardarServicios;//sirve para ir guardando los codigos de los servicios en el combox de servicos 002
        public string guardarEspecialidad;//lo mis mo que 002 pero para especialidades
        public static char SoloCuandoAccion;

        public static ArrayList CodigoEspecialiad = new ArrayList();//sirve para ir a guardar a la tabla de 
        public static ArrayList CodigoServicio = new ArrayList();


        public string CodigoTipo;
        public static string[] soloCodigoTipo;//servira para ver que tipo de empresa es la que se debe guardar en el formulario

        public string CodigoRegion;
        public static string[] SoloCodigoRegion;


        public string guardarRegion;
        public string guardarTipoEmpresa;
        public static char LlenarRegion;
        public static char LlenarTipoEmpresa;


        public static int ContadorIDEmpresas;
        public string CodigoEspecialidad;
        public static string[] SoloCodigoEspecialidad;

        public string CodigoServicios;
        public string [] SoloCodigoServicio;

        public string EstadoEmpresa;
        public string FechaCreacion;

        public int contadorServicios;
        public string CodigoEnviarSErvicio;

        MySqlConnection conexion2 = new MySqlConnection("server=localhost; Uid=root; password=fido123; database=proyectoipc2fase3");//sirve para conectarme con mi BD base datos

        protected void Page_Load(object sender, EventArgs e)
        {
            
            llenarComboBoxTiposEmpresas();
            LlenarComboBoxServicios();
            LlenarComboBoxRegion();
            LlenarComboBoxEspecialidades();
            EstadoEmpresa = "pre-evaluacion";
            SoloCuandoAccion = 'N';
            llenarIdQueSigue();
        }

        
        protected void BotonCargarFormulario_Click(object sender, EventArgs e)
        {
            FechaCreacion = DateTime.Now.ToString("yyyy-M-dd");//sirve para obtene la fecha actual de la creacion

            if (soloCodigoTipo[0].Equals("1"))//es para cuando sea hotel
            {
                if (!TextBoxNombreEmpresa.Text.Equals("") && !TextBoxTelefonoEmpresa.Text.Equals("") && !TextBoxDireccionEmpresa.Text.Equals(""))
                {

                    ContadorIDEmpresas++;
                    try
                    {

                        conexion2.Open();
                        MySqlCommand Agregar = new MySqlCommand("insert into empresa_turistica (id_empresa_turistic,tipo_empressa,nombre_empressa,direccion_empressa,telefono_empressa,email_empressa,region_empressa,estado_inscripcion_empres,fecha_creacion) values('" + ContadorIDEmpresas + "','" + soloCodigoTipo[0].ToString() + "','" + TextBoxNombreEmpresa.Text + "','" + TextBoxDireccionEmpresa.Text + "','" + TextBoxTelefonoEmpresa.Text + "','" + TextBoxEmailEmpresa.Text + "','" + SoloCodigoRegion[0].ToString() + "','" + EstadoEmpresa + "','" + FechaCreacion + "')", conexion2);
                        Agregar.ExecuteNonQuery();
                        conexion2.Close();

                        while (contadorServicios < CodigoServicio.Count)
                        {
                            CodigoEnviarSErvicio = CodigoServicio[contadorServicios].ToString();
                            conexion2.Open();

                            MySqlCommand GuardarGeneros = new MySqlCommand("insert into detalle_servicio (id_empresa,id_service) values('" + ContadorIDEmpresas + "','" + CodigoEnviarSErvicio + "')", conexion2);
                            GuardarGeneros.ExecuteNonQuery();
                            conexion2.Close();
                            contadorServicios++;
                        }

                        Response.Write("Se A Guardo La Informacion Correctamente");
                        contadorServicios = 0;

                        ComboBoxServiciosEmpresa.Items.Clear();
                        ComboBoxRegionEmpresa.Items.Clear();
                        ComboBoxTipoEmpresa.Items.Clear();
                        ComboBoxEspecialidadEmpresa.Items.Clear();


                        ComboBoxServiciosEmpresa.Items.Add("Seleccione_Servicios:");
                        ComboBoxRegionEmpresa.Items.Add("Seleccione_Region:");
                        ComboBoxTipoEmpresa.Items.Add("Seleccione_Tipo:");
                        ComboBoxEspecialidadEmpresa.Items.Add("Seleccione_Especialidad");


                        LlenarRegion = 'S';
                        LlenarEspecialidad = 'S';
                        LlenarTipoEmpresa = 'S';
                        LlenarServicios = 'S';

                        LlenarComboBoxEspecialidades();
                        LlenarComboBoxRegion();
                        llenarComboBoxTiposEmpresas();
                        LlenarComboBoxServicios();


                        TextBoxNombreEmpresa.Text = "";
                        TextBoxDireccionEmpresa.Text = "";
                        TextBoxEmailEmpresa.Text = "";
                        TextBoxTelefonoEmpresa.Text = "";
                        TextBoxHoraEmpresaIni.Text = "";
                        TextBoxHoraFinEmpre.Text = "";
                        TextBoxTarifaEmpresa.Text = "";

                        TextBoxHoraEmpresaIni.Enabled = true;
                        TextBoxHoraFinEmpre.Enabled = true;
                        TextBoxTarifaEmpresa.Enabled = true;
                        ComboBoxEspecialidadEmpresa.Enabled = true;
                        ComboBoxServiciosEmpresa.Enabled = true;



                        CodigoServicio.Clear();



                    }
                    catch (Exception)
                    {
                        conexion2.Close();
                        Response.Write("No Se Pudo Guardar La Informacion");
                        contadorServicios = 0;

                        ComboBoxServiciosEmpresa.Items.Clear();
                        ComboBoxRegionEmpresa.Items.Clear();
                        ComboBoxTipoEmpresa.Items.Clear();
                        ComboBoxEspecialidadEmpresa.Items.Clear();


                        ComboBoxServiciosEmpresa.Items.Add("Seleccione_Servicios:");
                        ComboBoxRegionEmpresa.Items.Add("Seleccione_Region:");
                        ComboBoxTipoEmpresa.Items.Add("Seleccione_Tipo:");
                        ComboBoxEspecialidadEmpresa.Items.Add("Seleccione_Especialidad");


                        LlenarRegion = 'S';
                        LlenarEspecialidad = 'S';
                        LlenarTipoEmpresa = 'S';
                        LlenarServicios = 'S';

                        LlenarComboBoxEspecialidades();
                        LlenarComboBoxRegion();
                        llenarComboBoxTiposEmpresas();
                        LlenarComboBoxServicios();


                        TextBoxNombreEmpresa.Text = "";
                        TextBoxDireccionEmpresa.Text = "";
                        TextBoxEmailEmpresa.Text = "";
                        TextBoxTelefonoEmpresa.Text = "";
                        TextBoxHoraEmpresaIni.Text = "";
                        TextBoxHoraFinEmpre.Text = "";
                        TextBoxTarifaEmpresa.Text = "";

                        TextBoxHoraEmpresaIni.Enabled = true;
                        TextBoxHoraFinEmpre.Enabled = true;
                        TextBoxTarifaEmpresa.Enabled = true;
                        ComboBoxEspecialidadEmpresa.Enabled = true;
                        ComboBoxServiciosEmpresa.Enabled = true;



                        CodigoServicio.Clear();

                    }


                }

            }

            else if (soloCodigoTipo[0].Equals("2"))
            {
                if (!TextBoxNombreEmpresa.Text.Equals("") && !TextBoxTelefonoEmpresa.Text.Equals("") && !TextBoxEmailEmpresa.Text.Equals("") && SoloCodigoEspecialidad.Length > 0)
                {


                    ContadorIDEmpresas++;
                    try
                    {

                        conexion2.Open();
                        MySqlCommand Agregar = new MySqlCommand("insert into empresa_turistica (id_empresa_turistic,tipo_empressa,nombre_empressa,direccion_empressa,telefono_empressa,email_empressa,region_empressa,horario_atencion,horario_fin_atencion,especialidad,estado_inscripcion_empres,fecha_creacion) values('" + ContadorIDEmpresas + "','" + soloCodigoTipo[0].ToString() + "','" + TextBoxNombreEmpresa.Text + "','" + TextBoxDireccionEmpresa.Text + "','" + TextBoxTelefonoEmpresa.Text + "','" + TextBoxEmailEmpresa.Text + "','" + SoloCodigoRegion[0].ToString() + "','" + TextBoxHoraEmpresaIni.Text + "','" + TextBoxHoraFinEmpre.Text + "','" + SoloCodigoEspecialidad[0].ToString() + "','" + EstadoEmpresa + "','" + FechaCreacion + "')", conexion2);
                        Agregar.ExecuteNonQuery();
                        conexion2.Close();


                        Response.Write("Se A Guardo La Informacion Correctamente");
                        contadorServicios = 0;

                        ComboBoxServiciosEmpresa.Items.Clear();
                        ComboBoxRegionEmpresa.Items.Clear();
                        ComboBoxTipoEmpresa.Items.Clear();
                        ComboBoxEspecialidadEmpresa.Items.Clear();


                        ComboBoxServiciosEmpresa.Items.Add("Seleccione_Servicios:");
                        ComboBoxRegionEmpresa.Items.Add("Seleccione_Region:");
                        ComboBoxTipoEmpresa.Items.Add("Seleccione_Tipo:");
                        ComboBoxEspecialidadEmpresa.Items.Add("Seleccione_Especialidad");


                        LlenarRegion = 'S';
                        LlenarEspecialidad = 'S';
                        LlenarTipoEmpresa = 'S';
                        LlenarServicios = 'S';

                        LlenarComboBoxEspecialidades();
                        LlenarComboBoxRegion();
                        llenarComboBoxTiposEmpresas();
                        LlenarComboBoxServicios();


                        TextBoxNombreEmpresa.Text = "";
                        TextBoxDireccionEmpresa.Text = "";
                        TextBoxEmailEmpresa.Text = "";
                        TextBoxTelefonoEmpresa.Text = "";
                        TextBoxHoraEmpresaIni.Text = "";
                        TextBoxHoraFinEmpre.Text = "";
                        TextBoxTarifaEmpresa.Text = "";

                        TextBoxHoraEmpresaIni.Enabled = true;
                        TextBoxHoraFinEmpre.Enabled = true;
                        TextBoxTarifaEmpresa.Enabled = true;
                        ComboBoxEspecialidadEmpresa.Enabled = true;
                        ComboBoxServiciosEmpresa.Enabled = true;







                    }
                    catch (Exception)
                    {
                        conexion2.Close();
                        Response.Write("No Se Pudo Guardar La Informacion");
                        contadorServicios = 0;

                        ComboBoxServiciosEmpresa.Items.Clear();
                        ComboBoxRegionEmpresa.Items.Clear();
                        ComboBoxTipoEmpresa.Items.Clear();
                        ComboBoxEspecialidadEmpresa.Items.Clear();


                        ComboBoxServiciosEmpresa.Items.Add("Seleccione_Servicios:");
                        ComboBoxRegionEmpresa.Items.Add("Seleccione_Region:");
                        ComboBoxTipoEmpresa.Items.Add("Seleccione_Tipo:");
                        ComboBoxEspecialidadEmpresa.Items.Add("Seleccione_Especialidad");


                        LlenarRegion = 'S';
                        LlenarEspecialidad = 'S';
                        LlenarTipoEmpresa = 'S';
                        LlenarServicios = 'S';

                        LlenarComboBoxEspecialidades();
                        LlenarComboBoxRegion();
                        llenarComboBoxTiposEmpresas();
                        LlenarComboBoxServicios();


                        TextBoxNombreEmpresa.Text = "";
                        TextBoxDireccionEmpresa.Text = "";
                        TextBoxEmailEmpresa.Text = "";
                        TextBoxTelefonoEmpresa.Text = "";
                        TextBoxHoraEmpresaIni.Text = "";
                        TextBoxHoraFinEmpre.Text = "";
                        TextBoxTarifaEmpresa.Text = "";

                        TextBoxHoraEmpresaIni.Enabled = true;
                        TextBoxHoraFinEmpre.Enabled = true;
                        TextBoxTarifaEmpresa.Enabled = true;
                        ComboBoxEspecialidadEmpresa.Enabled = true;
                        ComboBoxServiciosEmpresa.Enabled = true;



                        CodigoServicio.Clear();

                    }




                }

            }

            else if (soloCodigoTipo[0].Equals("3"))
            {

                if (!TextBoxNombreEmpresa.Text.Equals("") && !TextBoxTelefonoEmpresa.Text.Equals("") && !TextBoxTarifaEmpresa.Text.Equals(""))
                {

                    ContadorIDEmpresas++;
                    try
                    {

                        conexion2.Open();
                        MySqlCommand Agregar = new MySqlCommand("insert into empresa_turistica (id_empresa_turistic,tipo_empressa,nombre_empressa,direccion_empressa,telefono_empressa,email_empressa,region_empressa,horario_atencion,horario_fin_atencion,tarifa,estado_inscripcion_empres,fecha_creacion) values('" + ContadorIDEmpresas + "','" + soloCodigoTipo[0].ToString() + "','" + TextBoxNombreEmpresa.Text + "','" + TextBoxDireccionEmpresa.Text + "','" + TextBoxTelefonoEmpresa.Text + "','" + TextBoxEmailEmpresa.Text + "','" + SoloCodigoRegion[0].ToString() + "','" + TextBoxHoraEmpresaIni.Text + "','" + TextBoxHoraFinEmpre.Text + "','" + TextBoxTarifaEmpresa.Text + "','" + EstadoEmpresa + "','" + FechaCreacion + "')", conexion2);
                        Agregar.ExecuteNonQuery();
                        conexion2.Close();


                        Response.Write("Se A Guardo La Informacion Correctamente");
                        contadorServicios = 0;

                        ComboBoxServiciosEmpresa.Items.Clear();
                        ComboBoxRegionEmpresa.Items.Clear();
                        ComboBoxTipoEmpresa.Items.Clear();
                        ComboBoxEspecialidadEmpresa.Items.Clear();


                        ComboBoxServiciosEmpresa.Items.Add("Seleccione_Servicios:");
                        ComboBoxRegionEmpresa.Items.Add("Seleccione_Region:");
                        ComboBoxTipoEmpresa.Items.Add("Seleccione_Tipo:");
                        ComboBoxEspecialidadEmpresa.Items.Add("Seleccione_Especialidad");


                        LlenarRegion = 'S';
                        LlenarEspecialidad = 'S';
                        LlenarTipoEmpresa = 'S';
                        LlenarServicios = 'S';

                        LlenarComboBoxEspecialidades();
                        LlenarComboBoxRegion();
                        llenarComboBoxTiposEmpresas();
                        LlenarComboBoxServicios();


                        TextBoxNombreEmpresa.Text = "";
                        TextBoxDireccionEmpresa.Text = "";
                        TextBoxEmailEmpresa.Text = "";
                        TextBoxTelefonoEmpresa.Text = "";
                        TextBoxHoraEmpresaIni.Text = "";
                        TextBoxHoraFinEmpre.Text = "";
                        TextBoxTarifaEmpresa.Text = "";

                        TextBoxHoraEmpresaIni.Enabled = true;
                        TextBoxHoraFinEmpre.Enabled = true;
                        TextBoxTarifaEmpresa.Enabled = true;
                        ComboBoxEspecialidadEmpresa.Enabled = true;
                        ComboBoxServiciosEmpresa.Enabled = true;

                            





                    }
                    catch (Exception)
                    {
                        conexion2.Close();
                        Response.Write("No Se Pudo Guardar La Informacion");
                        contadorServicios = 0;

                        ComboBoxServiciosEmpresa.Items.Clear();
                        ComboBoxRegionEmpresa.Items.Clear();
                        ComboBoxTipoEmpresa.Items.Clear();
                        ComboBoxEspecialidadEmpresa.Items.Clear();


                        ComboBoxServiciosEmpresa.Items.Add("Seleccione_Servicios:");
                        ComboBoxRegionEmpresa.Items.Add("Seleccione_Region:");
                        ComboBoxTipoEmpresa.Items.Add("Seleccione_Tipo:");
                        ComboBoxEspecialidadEmpresa.Items.Add("Seleccione_Especialidad");


                        LlenarRegion = 'S';
                        LlenarEspecialidad = 'S';
                        LlenarTipoEmpresa = 'S';
                        LlenarServicios = 'S';

                        LlenarComboBoxEspecialidades();
                        LlenarComboBoxRegion();
                        llenarComboBoxTiposEmpresas();
                        LlenarComboBoxServicios();


                        TextBoxNombreEmpresa.Text = "";
                        TextBoxDireccionEmpresa.Text = "";
                        TextBoxEmailEmpresa.Text = "";
                        TextBoxTelefonoEmpresa.Text = "";
                        TextBoxHoraEmpresaIni.Text = "";
                        TextBoxHoraFinEmpre.Text = "";
                        TextBoxTarifaEmpresa.Text = "";

                        TextBoxHoraEmpresaIni.Enabled = true;
                        TextBoxHoraFinEmpre.Enabled = true;
                        TextBoxTarifaEmpresa.Enabled = true;
                        ComboBoxEspecialidadEmpresa.Enabled = true;
                        ComboBoxServiciosEmpresa.Enabled = true;



                        CodigoServicio.Clear();

                    }


                }

            }

            //hacer lo de la practica 3 de agregar aqui y traer llenar los servicios y especialidades de la base de datos

            //HACER LO DE AGREGAR 
        }




     


        protected void ComboBoxRegionEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CodigoRegion = ComboBoxRegionEmpresa.SelectedItem.Text;
            SoloCodigoRegion = CodigoRegion.Split('-');
            //solo para tener el id de la region que selecciono
            ComboBoxRegionEmpresa.Items.Remove(CodigoRegion);
            LlenarRegion = 'N';
            LlenarEspecialidad = 'N';
            LlenarTipoEmpresa = 'N';
            LlenarServicios = 'N';
            SoloCuandoAccion = 'N';


        }


        protected void ComboBoxEspecialidadEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CodigoEspecialidad = ComboBoxEspecialidadEmpresa.SelectedItem.Text;
            SoloCodigoEspecialidad = CodigoEspecialidad.Split('-');
            ComboBoxEspecialidadEmpresa.Items.Remove(CodigoEspecialidad);
            SoloCuandoAccion = 'N';
        }


        protected void ComboBoxServiciosEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

            CodigoServicios = ComboBoxServiciosEmpresa.SelectedItem.Text;
            SoloCodigoServicio= CodigoServicios.Split('-');
            CodigoServicio.Add(SoloCodigoServicio[0].ToString());
            ComboBoxServiciosEmpresa.Items.Remove(CodigoServicios);
            SoloCuandoAccion = 'N';//guardara todo los codigos que seleccione del hotel osea los servicios
        }



        
        public void llenarComboBoxTiposEmpresas()
        {

            if (SoloCuandoAccion == 'S' || LlenarTipoEmpresa == 'S')
            {
                conexion2.Open();
                MySqlCommand Sesion = new MySqlCommand("select *from tipo", conexion2);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = Sesion;
                DataTable tabla = new DataTable();
                inicio.Fill(tabla);
                conexion2.Close();

                for (int i = 0; i < tabla.Rows.Count; i++)
                {//añado los codigos y nombres de los generos al combox

                    guardarTipoEmpresa = tabla.Rows[i]["id_tipo"].ToString();
                    guardarTipoEmpresa = guardarTipoEmpresa + "-" + tabla.Rows[i]["nombre_tipo"].ToString();
                    guardarTipoEmpresa = guardarTipoEmpresa.Substring(0, guardarTipoEmpresa.Length);
                    ComboBoxTipoEmpresa.Items.Add(guardarTipoEmpresa);
                    guardarTipoEmpresa = "";

                }
            }
            LlenarTipoEmpresa = 'N';

        }

        public void LlenarComboBoxRegion()
        {

            if (SoloCuandoAccion == 'S' || LlenarRegion == 'S')
            {
                conexion2.Open();
                MySqlCommand Sesion = new MySqlCommand("select *from region", conexion2);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = Sesion;
                DataTable tabla = new DataTable();
                inicio.Fill(tabla);
                conexion2.Close();

                for (int i = 0; i < tabla.Rows.Count; i++)
                {//añado los codigos y nombres de los generos al combox

                    guardarRegion = tabla.Rows[i]["codigo_regionn"].ToString();
                    guardarRegion = guardarRegion + "-" + tabla.Rows[i]["nombre_regionn"].ToString();
                    guardarRegion = guardarRegion.Substring(0, guardarRegion.Length);
                    ComboBoxRegionEmpresa.Items.Add(guardarRegion);
                    guardarRegion = "";

                }
            }
            LlenarRegion = 'N';
        }


        public void LlenarComboBoxEspecialidades()
        {
            if (SoloCuandoAccion == 'S' || LlenarEspecialidad == 'S')
            {
                conexion2.Open();
                MySqlCommand Sesion = new MySqlCommand("select *from especialidad", conexion2);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = Sesion;
                DataTable tabla = new DataTable();
                inicio.Fill(tabla);
                conexion2.Close();

                for (int i = 0; i < tabla.Rows.Count; i++)
                {//añado los codigos y nombres de los generos al combox

                    guardarEspecialidad = tabla.Rows[i]["id_especialidad"].ToString();
                    guardarEspecialidad = guardarEspecialidad + "-" + tabla.Rows[i]["nombre_especialidad"].ToString();
                    guardarEspecialidad = guardarEspecialidad.Substring(0, guardarEspecialidad.Length);
                    ComboBoxEspecialidadEmpresa.Items.Add(guardarEspecialidad);
                    guardarEspecialidad = "";

                }
            }
            LlenarEspecialidad = 'N';
        }

        public void LlenarComboBoxServicios()
        {
            if (SoloCuandoAccion == 'S' || LlenarServicios == 'S')
            {
                conexion2.Open();
                MySqlCommand Sesion = new MySqlCommand("select *from catalogo_servicio", conexion2);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = Sesion;
                DataTable tabla = new DataTable();
                inicio.Fill(tabla);
                conexion2.Close();

                for (int i = 0; i < tabla.Rows.Count; i++)
                {//añado los codigos y nombres de los generos al combox

                    guardarServicios = tabla.Rows[i]["id_servicio_catalogo"].ToString();
                    guardarServicios = guardarServicios + "-" + tabla.Rows[i]["nombre_servicio_catalogo"].ToString();
                    guardarServicios = guardarServicios.Substring(0, guardarServicios.Length);
                    ComboBoxServiciosEmpresa.Items.Add(guardarServicios);
                    guardarServicios = "";

                }
            }
            LlenarServicios = 'N';
        }

        protected void ComboBoxTipoEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

            CodigoTipo = ComboBoxTipoEmpresa.SelectedItem.Text;
            soloCodigoTipo = CodigoTipo.Split('-');
            ComboBoxTipoEmpresa.Items.Remove(CodigoTipo);

            if (soloCodigoTipo[0].Equals("1"))//significa que es hotel
            {


                //LabelServiciosHotel.Visible = true;
                ////ComboBoxServiciosEmpresa.Visible = true;

                //TextBoxHoraEmpresaIni. = true;
                //TextBoxHoraFinEmpre.Visible = true;
                //ComboBoxEspecialidadEmpresa.Visible = true;
                ComboBoxTipoEmpresa.Items.Clear();
                ComboBoxTipoEmpresa.Items.Add("Empresa Tipo: " + CodigoTipo);

            }


            else if (soloCodigoTipo[0].Equals("2"))//significa que es restaurante
            {
                //LabelHoraAtencionInicio.Visible = true;
                //LabelHoraAtencionFin.Visible = true;
                //LabelEspecialidadRestaurante.Visible = true;

                //TextBoxHoraEmpresaIni.Visible = true;
                //TextBoxHoraFinEmpre.Visible = true;
                //ComboBoxEspecialidadEmpresa.Visible = true;

                ComboBoxTipoEmpresa.Items.Clear();
                ComboBoxTipoEmpresa.Items.Add("Empresa Tipo: " + CodigoTipo);


            }

            else if (soloCodigoTipo[0].Equals("3"))//significa que es museo
            {
                //LabelHoraAtencionInicio.Visible = true;
                //LabelHoraAtencionFin.Visible = true;
                //LabelTarifaMuseo.Visible = true;
                //TextBoxHoraEmpresaIni.Visible = true;
                //TextBoxHoraFinEmpre.Visible = true;
                //TextBoxTarifaEmpresa.Visible = true;
                ComboBoxTipoEmpresa.Items.Clear();
                ComboBoxTipoEmpresa.Items.Add("Empresa Tipo: " + CodigoTipo);



            }
            CodigoTipo = "";

        }

        public void llenarIdQueSigue()
        {

            try
            {
                conexion2.Open();
                MySqlCommand ExtraerDatosModDelete = new MySqlCommand("select *from empresa_turistica", conexion2);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = ExtraerDatosModDelete;
                DataTable tablaModEli = new DataTable();
                inicio.Fill(tablaModEli);
                conexion2.Close();

                for (int ss = 0; ss < tablaModEli.Rows.Count; ss++)//pondra en los text box la informacion a modificar
                {

                    if (ss + 1 == tablaModEli.Rows.Count)
                    {
                        ContadorIDEmpresas = int.Parse(tablaModEli.Rows[ss]["id_empresa_turistic"].ToString());
                    }
                }

            }

            catch (Exception)
            {

                conexion2.Close();
            }
        }
    }
}