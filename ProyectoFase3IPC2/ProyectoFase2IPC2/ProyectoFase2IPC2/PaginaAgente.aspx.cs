﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaAgente : System.Web.UI.Page
    {
       public static string idEmpresa;
        public string CodigoTecnico;
        public static string[] CodEnviarTablTecnico;
        public string CodigoRegion;
        public static string[] CodigoTraerRegion;
        public string estadoinscripcion;
        public string EstadoInscripcionEvaluacion;

        public int CodigoReciboEmpresa;
        public int CodigoEvaluacionEmpresa;
        public int NumeroCorrelativo;
        public string precioPagarEmpresa;

        MySqlConnection conexion3 = new MySqlConnection("server=localhost; Uid=root; password=fido123; database=proyectoipc2fase3");
        protected void Page_Load(object sender, EventArgs e)
        {
            ComboxTecnico.Enabled = false;
            estadoinscripcion = "pre-inscrita";
            EstadoInscripcionEvaluacion = "evaluacion";
            precioPagarEmpresa = "200.00";
            ObtenerUltimoIdEvaluacion();
            ObtenerUltimoReciboYEvaluacion();

        }

        protected void BotonCerrarSesion_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaginaPrincipal.aspx");
            this.Dispose();
        }

        protected void ComboBoxTipoInscripcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxTipoInscripcion.SelectedItem.Text.Equals("SitioTuristico"))
            {
                ComboxTecnico.Enabled=false;
                PaginaFormularioIncripcion.SoloCuandoAccion = 'S';//para que llene el combox de las regiones
                string _open = "window.open('PaginaFormularioIncripcion.aspx', '_newtab');";//sirve para abrir en una nueva pestaña otra ventana o web form
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);


            }//hay que habilitar en las opciones del DropDownListe en el diseno web la opcion AutoPostBack para que funcione el evento de seleccion

            else if (ComboBoxTipoInscripcion.SelectedItem.Text.Equals("EmpresaTuristica"))
            {
                ComboxTecnico.Enabled = true;
                ComboxTecnico.Items.Clear();
                ComboxTecnico.Items.Add("Seleccionar Tecnico");
                LlenarComboxDeTecnicosDisponibles();


            }
        }


        public void LlenarComboxDeTecnicosDisponibles()
        {
            conexion3.Open();
            MySqlCommand Sesion = new MySqlCommand("select *from usuario_administracion", conexion3);
            MySqlDataAdapter inicio = new MySqlDataAdapter();
            inicio.SelectCommand = Sesion;
            DataTable tabla = new DataTable();
            inicio.Fill(tabla);
            conexion3.Close();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {//añado los codigos de los usuarios al combox 

                if (tabla.Rows[i]["rol_usuario"].ToString().Equals("tecnico"))
                {
                    CodigoTecnico = tabla.Rows[i]["id_usuario"].ToString();
                    CodigoTecnico = CodigoTecnico + "-" + tabla.Rows[i]["nombre_usuario"].ToString();
                    CodigoTecnico = CodigoTecnico.Substring(0, CodigoTecnico.Length);
                    ComboxTecnico.Items.Add(CodigoTecnico);
                }//ASI ME ASEGURO QUE SOLO ID DE TECNICOS VA LLENAR EN EL COMBOX
              

            }

        }

        protected void ComboxSeleccionarCodigoUser_SelectedIndexChanged(object sender, EventArgs e)//ESTE ES EL CODIGO DE LA REGION QUE DESEO RECUPERAR LOS FORMULARIOS
        {
            CodigoRegion = ComboxSeleccionarCodigRegion.SelectedItem.Text;
            CodigoTraerRegion = CodigoRegion.Split('-');
            
        }

        protected void BuscarFormulariosParaInscripcion_Click(object sender, EventArgs e)
        {
            

            try
            {
                conexion3.Open();//poner esto si no quiero que se vena las que dicen en estado de evaluacion +"and estado_inscripcion_empres='"+estadoinscripcion+"'"
                MySqlCommand Sesion = new MySqlCommand("select *from empresa_turistica where region_empressa='"+CodigoTraerRegion[0].ToString()+"'", conexion3);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = Sesion;
                DataTable tabla = new DataTable();
                inicio.Fill(tabla);
                conexion3.Close();
                TablaFormularios.DataSource = tabla;
                TablaFormularios.DataBind();




            }
            catch (Exception)
            {
                conexion3.Close();
                Response.Write("NO HAY REGISTROS DE ESA REGION");
            }


        }

        protected void TablaFormularios_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = TablaFormularios.SelectedRow;

             idEmpresa = row.Cells[1].Text.ToString();

            //aqui poner le le vaya a poner a estado de la empresa en Evaluacion 
            
        }


        public void ObtenerUltimoReciboYEvaluacion()
        {

            try
            {
                conexion3.Open();
                MySqlCommand ExtraerDatosModDelete = new MySqlCommand("select *from recibo_empresa", conexion3);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = ExtraerDatosModDelete;
                DataTable tablaModEli = new DataTable();
                inicio.Fill(tablaModEli);
                conexion3.Close();

                for (int ss = 0; ss < tablaModEli.Rows.Count; ss++)//pondra en los text box la informacion a modificar
                {

                    if (ss + 1 == tablaModEli.Rows.Count)
                    {
                        CodigoReciboEmpresa = int.Parse(tablaModEli.Rows[ss]["id_recibo"].ToString());
                    }
                }

            }

            catch (Exception)
            {

                conexion3.Close();
            }


        }

        public void ObtenerUltimoIdEvaluacion()
        {

            try
            {
                conexion3.Open();
                MySqlCommand ExtraerDatosModDelete = new MySqlCommand("select *from evaluacion_tecnica", conexion3);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = ExtraerDatosModDelete;
                DataTable tablaModEli = new DataTable();
                inicio.Fill(tablaModEli);
                conexion3.Close();

                for (int ss = 0; ss < tablaModEli.Rows.Count; ss++)//pondra en los text box la informacion a modificar
                {

                    if (ss + 1 == tablaModEli.Rows.Count)
                    {
                        CodigoEvaluacionEmpresa = int.Parse(tablaModEli.Rows[ss]["id_evaluacion_tecnic"].ToString());
                    }
                }

            }

            catch (Exception)
            {

                conexion3.Close();
            }

        }

        protected void ComboxTecnico_SelectedIndexChanged(object sender, EventArgs e)//srive para obtener el codigo del tecnico que se le debe asignar la empresa
        {
            CodigoTecnico = ComboxTecnico.SelectedItem.Text;
            CodEnviarTablTecnico = CodigoTecnico.Split('-');
        }

        protected void Cargar_Sitio_Empresa_Click(object sender, EventArgs e)
        {
            try
            {
                conexion3.Open();
                MySqlCommand Modificacion = new MySqlCommand("update empresa_turistica set estado_inscripcion_empres='" + EstadoInscripcionEvaluacion + "'where id_empresa_turistic='" + idEmpresa + "'", conexion3);
                Modificacion.ExecuteNonQuery();
                conexion3.Close();
                Response.Write("Se A Modificado Con Exito");

            }
            catch (Exception)
            {

                conexion3.Close();
                Response.Write("No se A Modificado");
            }


            try
            {
                CodigoReciboEmpresa++;
                NumeroCorrelativo = CodigoReciboEmpresa + 1;
                conexion3.Open();
                MySqlCommand Agregar = new MySqlCommand("insert into recibo_empresa (id_recibo,numero_correlativo,precio_pagar,id_empresa_recibo) values('" + CodigoReciboEmpresa + "','" + NumeroCorrelativo + "','" + precioPagarEmpresa + "','" + idEmpresa + "')", conexion3);
                Agregar.ExecuteNonQuery();
                conexion3.Close();
                Response.Write("Se A Guardo La Informacion Correctamente");

            }
            catch (Exception)
            {
                conexion3.Close();
                
            }



            try
            {
                CodigoEvaluacionEmpresa++;
               
                conexion3.Open();
                MySqlCommand Agregar = new MySqlCommand("insert into evaluacion_tecnica (id_evalucion_tecnic,numero_recibo_evaluacion,id_usuario_evaluacion1) values('" + CodigoEvaluacionEmpresa + "','" + CodigoReciboEmpresa + "','" +CodEnviarTablTecnico[0].ToString()  + "')", conexion3);
                Agregar.ExecuteNonQuery();
                conexion3.Close();
                Response.Write("Se A Guardo La Informacion Correctamente");
             
            }
            catch (Exception)
            {
                conexion3.Close();
                
            }

        }
    }
}