﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaFormularioIncripcion : System.Web.UI.Page
    {
        MySqlConnection conexion2 = new MySqlConnection("server=localhost; Uid=root; password=fido123; database=proyectoipc2fase3");//sirve para conectarme con mi BD base datos
        public static int contadorSitiosTuristico;
        public static string EstadoInscripcion;
        public string FechaCreacion;
        public static char SoloCuandoAccion;
        public char LlenarRegion;
        public string guardarRegion;

        public string CodigoRegion;
        public static string[] SoloCodigoRegion;

        protected void Page_Load(object sender, EventArgs e)
        {
            llenarComboxRegion();
            EstadoInscripcion = "inscrita";
            SoloCuandoAccion = 'N';
            ObtenerNumeroidSiguiente();
        }

        protected void BotonCargarFormulario_Click(object sender, EventArgs e)
        {
            


            if (!TextBoxNombreSitio.Text.Equals(""))
            {

                try
                {

                    contadorSitiosTuristico++;
                    conexion2.Open();
                    MySqlCommand Agregar = new MySqlCommand("insert into sitio_turistico (id_sitio_turistic,nombre_sitio_turistic,descripcion_sitio_turistic,region_sitio_turistic,estado_inscripcion) values('" + contadorSitiosTuristico + "','" + TextBoxNombreSitio.Text + "','" + TextBoxDescripcionSitio.Text + "','" + SoloCodigoRegion[0].ToString() + "','" + EstadoInscripcion + "')", conexion2);
                    Agregar.ExecuteNonQuery();
                    conexion2.Close();
                    Response.Write("Se A Guardo La Informacion Correctamente");
                    ComboBoxRegionSitio.Items.Clear();
                    ComboBoxRegionSitio.Items.Add("Seleccione_Region:");
                    LlenarRegion = 'S';
                    llenarComboxRegion();

                    TextBoxNombreSitio.Text = "";
                    TextBoxDescripcionSitio.Text = "";
                    



                }
                catch (Exception)
                {
                    conexion2.Close();
                    Response.Write("No Se Pudo Guardar La Informacion");
                    ComboBoxRegionSitio.Items.Clear();
                    ComboBoxRegionSitio.Items.Add("Seleccione_Region:");
                    LlenarRegion = 'S';
                    llenarComboxRegion();

                    TextBoxNombreSitio.Text = "";
                    TextBoxDescripcionSitio.Text = "";

                }
                
            }



        }


        public void ObtenerNumeroidSiguiente()
        {

            try { 
            conexion2.Open();
            MySqlCommand ExtraerDatosModDelete = new MySqlCommand("select *from sitio_turistico", conexion2);
            MySqlDataAdapter inicio = new MySqlDataAdapter();
            inicio.SelectCommand = ExtraerDatosModDelete;
            DataTable tablaModEli = new DataTable();
            inicio.Fill(tablaModEli);
            conexion2.Close();

            for (int ss = 0; ss < tablaModEli.Rows.Count; ss++)//pondra en los text box la informacion a modificar
            {

                    if (ss+1==tablaModEli.Rows.Count)
                    {
                        contadorSitiosTuristico = int.Parse(tablaModEli.Rows[ss]["id_sitio_turistic"].ToString());
                    }
            }

        }

                    catch (Exception)
            {

                conexion2.Close();
            }


        }








        public void llenarComboxRegion()
        {

            if (SoloCuandoAccion == 'S' || LlenarRegion == 'S')
            {
                conexion2.Open();
                MySqlCommand Sesion = new MySqlCommand("select *from region", conexion2);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = Sesion;
                DataTable tabla = new DataTable();
                inicio.Fill(tabla);
                conexion2.Close();

                for (int i = 0; i < tabla.Rows.Count; i++)
                {//añado los codigos y nombres de los generos al combox

                    guardarRegion = tabla.Rows[i]["codigo_regionn"].ToString();
                    guardarRegion = guardarRegion + "-" + tabla.Rows[i]["nombre_regionn"].ToString();
                    guardarRegion = guardarRegion.Substring(0, guardarRegion.Length);
                    ComboBoxRegionSitio.Items.Add(guardarRegion);
                    guardarRegion = "";

                    
                }
            }
            LlenarRegion = 'N';
        }

        protected void ComboBoxRegionSitio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CodigoRegion = ComboBoxRegionSitio.SelectedItem.Text;
            SoloCodigoRegion = CodigoRegion.Split('-');
            //solo para tener el id de la region que selecciono
            ComboBoxRegionSitio.Items.Remove(CodigoRegion);
        }

        protected void CargarFotosBaseDatos_Click(object sender, EventArgs e)
        {
            //Variable en donde se almacena los archivos seleccionados
            HttpFileCollection multipleFiles = Request.Files;

            //leer cada archivo seleccionado
            for (int fileCount = 0; fileCount < multipleFiles.Count; fileCount++)
            {
                //archivo seleccionado
                HttpPostedFile uploadedFile = multipleFiles[fileCount];
                //tamaño de archivo: uploadedFile.ContentLength
                //3097152 = 3MB

                //nombre de archivo seleccionado
                //para usar Path.GetFileName, usar la referencia: using System.IO;
                string fileName = Path.GetFileName(uploadedFile.FileName);
                string file = Path.GetFullPath(uploadedFile.FileName);
                //si el archivo tiene un tamaño mayor a cero, subir
                if (uploadedFile.ContentLength > 0)
                {
                    ////subir el archivo al servidor
                    //uploadedFile.SaveAs(Server.MapPath("~/Files/") + fileName);

                    ////mostrar el archivo se ha subido
                    //Label1.Text += fileName + "Saved <BR>";



                    //string Raiz = @"C:\CargaMasivas\";
                    //string Raiz2 = "C:/CargaMasivas/";

                   
                    //    Raiz += uploadedFile.FileName.ToString();
                    //    Raiz2 += uploadedFile.FileName.ToString();//aqui tendria el path forma diagonal normal /

                       
                    //    ComandoCargaMasiva = "LOAD DATA LOCAL INFILE " + "'" + Raiz2 + "'" + "\n" + " INTO TABLE " + TablaIngresoDatos + "\n" + " COLUMNS TERMINATED BY " + "','" + "\n" + " LINES TERMINATED BY" + "'\n'";

                    //    try
                    //    {
                    //        conexion.Open();

                    //        MySqlCommand CargaMasiva = new MySqlCommand(ComandoCargaMasiva, conexion);


                    //        CargaMasiva.ExecuteNonQuery();
                    //        conexion.Close();
                    //        Response.Write("Se ha realizado con exito la carga masiva a la tabla seleccionada");
                    //    }
                    //    catch (Exception)
                    //    {

                    //        Response.Write("No Se Realizo La Carga Masivo De La Tabla que Selecciono");
                    //    }







                }
            }
        }
    }
}