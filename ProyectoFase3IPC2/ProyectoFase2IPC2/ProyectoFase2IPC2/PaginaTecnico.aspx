﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaTecnico.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaTecnico" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Tecnico-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoTecnico">
    <form id="form1" runat="server">
        <div>
    <h1  class="OtroH11" style="text-align:center">MODULO TECNICO
        
            </h1>
            <div class="OtroH11">   
                    
                    <asp:Button ID="ButtonCerrarSessionTec" class="Botones3C" runat="server" Text="Cerrar Sesion" OnClick="ButtonCerrarSessionTec_Click" Width="240px" />

        </div>
            <div class="OtroH11">
            <h1  class="OtroH11">
                <asp:DropDownList ID="ComboxSeleccionarEmpresa" runat="server" Height="58px" Width="208px" style="margin-left: 0px">
                    <asp:ListItem>Seleccionar_Empresa</asp:ListItem>
                </asp:DropDownList>
&nbsp;
                <asp:Button ID="BuscarEmpresaEvaluar" runat="server" class="BotonesC" Text="OBTENER EMPRESA" Width="250px" OnClick="BuscarEmpresaEvaluar_Click" />
            &nbsp;<asp:Button ID="BotonVisitarEmpresas" runat="server" class="BotonesC" Text="ASIGNAR VISITA" OnClick="BotonVisitarEmpresas_Click" Width="250px" />
            &nbsp;<asp:Button ID="BotonEvaluarVisitaTecnica" runat="server" class="BotonesC" Text="VISITA TECNICA" OnClick="BotonEvaluarVisitaTecnica_Click" Width="250px" />
            </h1>
                </div>
           


            <br />

            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4">
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                <SortedDescendingHeaderStyle BackColor="#7E0000" />
            </asp:GridView>

        </div>
    <div class="OtroH11">
 
        <br />
        <br />
        <br />
        

        
        <asp:Button ID="Cargar_Empresa_Web" runat="server" class="BotonesC" Text="APROBAR" />
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Button ID="Button1" runat="server" class="Botones2C" Text="RECHAZAR" Width="212px" />
        &nbsp;&nbsp;<br />
        <br />

        <br />

    </div>
       


    


    </form>
   
    

    

</body>

</html>
