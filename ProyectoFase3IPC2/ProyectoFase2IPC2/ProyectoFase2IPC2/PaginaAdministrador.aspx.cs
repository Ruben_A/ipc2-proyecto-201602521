﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class PaginaAdministrador : System.Web.UI.Page
    {
        public string CodigoNoModificableUSer;
        MySqlConnection conexion3 = new MySqlConnection("server=localhost; Uid=root; password=fido123; database=proyectoipc2fase3");
        public static string RolUsurio;
        protected void Page_Load(object sender, EventArgs e)
        {
            LlenarComboxUser();
        }

        protected void ButtonCerrarSesionAdmon_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaginaPrincipal.aspx");
            this.Dispose();
        }

        protected void CrearUser_Click(object sender, EventArgs e)
        {

            try
            {
                conexion3.Open();
                MySqlCommand Agregar = new MySqlCommand("insert into usuario_administracion (id_usuario,nombre_usuario,dpi_usuario,telefono_usuario,email_usuario,nombre_usuario_loguin,contrasena_usuario,rol_usuario) values('" + TextBoxIdusuario.Text + "','" + TextBoxNombreAdmon.Text + "','" + TextBoxDpiUsuarioAdmon.Text + "','" + TextBoxTelefonoAdmon.Text + "','" + TextBoxEmailAdmon.Text + "','" + TextBoxUserAdmon.Text + "','" + TextBoxPasswordAdmon.Text + "','" + RolUsurio+ "')", conexion3);
                Agregar.ExecuteNonQuery();
                conexion3.Close();
                Response.Write("Se A Guardo La Informacion Correctamente");
                ComboxSeleccionarCodigoUser.Items.Clear();//la vacio 
                ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");//le añado mensaje
                LlenarComboxUser();//la vuelvo a llenar el combox de los id´s

                TextBoxIdusuario.Text = "";

                TextBoxNombreAdmon.Text = "";
                TextBoxDpiUsuarioAdmon.Text = "";
                TextBoxTelefonoAdmon.Text = "";
                TextBoxEmailAdmon.Text = "";
                TextBoxUserAdmon.Text = "";
                TextBoxPasswordAdmon.Text = "";
                RolUsurio = "";

                TextBoxIdusuario.Enabled = true;


            }
            catch (Exception)
            {

                Response.Write("No Se Pudo Guardar La Informacion");
                ComboxSeleccionarCodigoUser.Items.Clear();//la vacio 
                ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");//le añado mensaje
                LlenarComboxUser();//la vuelvo a llenar el combox de los id´s

                TextBoxIdusuario.Text = "";

                TextBoxNombreAdmon.Text = "";
                TextBoxDpiUsuarioAdmon.Text = "";
                TextBoxTelefonoAdmon.Text = "";
                TextBoxEmailAdmon.Text = "";
                TextBoxUserAdmon.Text = "";
                TextBoxPasswordAdmon.Text = "";
                RolUsurio = "";

                TextBoxIdusuario.Enabled = true;

            }



        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxRoles.SelectedItem.Text.Equals("agente"))
            {
                RolUsurio = ComboBoxRoles.SelectedItem.Text;
            }

            else if (ComboBoxRoles.SelectedItem.Text.Equals("tecnico"))
            {
                RolUsurio = ComboBoxRoles.SelectedItem.Text;
            }

        }




        public void LlenarComboxUser()
        {

            conexion3.Open();
            MySqlCommand Sesion = new MySqlCommand("select *from usuario_administracion", conexion3);
            MySqlDataAdapter inicio = new MySqlDataAdapter();
            inicio.SelectCommand = Sesion;
            DataTable tabla = new DataTable();
            inicio.Fill(tabla);
            conexion3.Close();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {//añado los codigos de los usuarios al combox 

                ComboxSeleccionarCodigoUser.Items.Add(tabla.Rows[i]["id_usuario"].ToString());

            }



        }

        protected void BuscarIDUser_Click(object sender, EventArgs e)
        {
            CodigoNoModificableUSer = "";

            CodigoNoModificableUSer = ComboxSeleccionarCodigoUser.SelectedItem.Text;
            ComboxSeleccionarCodigoUser.Items.Clear();
            ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");
            LlenarComboxUser();

            try
            {
                conexion3.Open();
                MySqlCommand ExtraerDatosModDelete = new MySqlCommand("select *from usuario_administracion", conexion3);
                MySqlDataAdapter inicio = new MySqlDataAdapter();
                inicio.SelectCommand = ExtraerDatosModDelete;
                DataTable tablaModEli = new DataTable();
                inicio.Fill(tablaModEli);
                conexion3.Close();

                for (int ss = 0; ss < tablaModEli.Rows.Count; ss++)//pondra en los text box la informacion a modificar
                {
                    if (tablaModEli.Rows[ss]["id_usuario"].ToString().Equals(CodigoNoModificableUSer))
                    {

                        TextBoxIdusuario.Text = tablaModEli.Rows[ss]["id_usuario"].ToString();
                        TextBoxNombreAdmon.Text = tablaModEli.Rows[ss]["nombre_usuario"].ToString();
                        TextBoxDpiUsuarioAdmon.Text = tablaModEli.Rows[ss]["dpi_usuario"].ToString();
                        TextBoxTelefonoAdmon.Text = tablaModEli.Rows[ss]["telefono_usuario"].ToString();
                        TextBoxEmailAdmon.Text = tablaModEli.Rows[ss]["email_usuario"].ToString();
                        TextBoxUserAdmon.Text = tablaModEli.Rows[ss]["nombre_usuario_loguin"].ToString();
                        TextBoxPasswordAdmon.Text = tablaModEli.Rows[ss]["contrasena_usuario"].ToString();
                        LabelRolModificar.Text = "Rol_Actual: "+tablaModEli.Rows[ss]["rol_usuario"].ToString();
                        TextBoxIdusuario.Enabled = false;

                        //,,,
                    }


                }



            }
            catch (Exception)
            {

                conexion3.Close();
            }

        }

        protected void ModificarUser_Click(object sender, EventArgs e)
        {

            try
            {

                conexion3.Open();
                MySqlCommand Modificacion = new MySqlCommand("update usuario_administracion set nombre_usuario='" + TextBoxNombreAdmon.Text + "',dpi_usuario='" + TextBoxDpiUsuarioAdmon.Text + "',telefono_usuario='" + TextBoxTelefonoAdmon.Text + "',email_usuario='" + TextBoxEmailAdmon.Text + "',nombre_usuario_loguin='" + TextBoxUserAdmon.Text + "',contrasena_usuario='" + TextBoxPasswordAdmon.Text + "',rol_usuario='" + RolUsurio +  "'where id_usuario='" + TextBoxIdusuario.Text + "'", conexion3);
                Modificacion.ExecuteNonQuery();
                conexion3.Close();
                Response.Write("Se A Modificado Con Exito");

                ComboxSeleccionarCodigoUser.Items.Clear();//la vacio 
                ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");//le añado mensaje
                LlenarComboxUser();//la vuelvo a llenar el combox de los id´s

                TextBoxIdusuario.Text = "";

                TextBoxNombreAdmon.Text = "";
                TextBoxDpiUsuarioAdmon.Text = "";
                TextBoxTelefonoAdmon.Text = "";
                TextBoxEmailAdmon.Text = "";
                TextBoxUserAdmon.Text = "";
                TextBoxPasswordAdmon.Text = "";
                RolUsurio = "";

                TextBoxIdusuario.Enabled = true;
                LabelRolModificar.Text = "Modificacion:";

            }
            catch (Exception)
            {
                conexion3.Close();
                Response.Write("No Se A Modificado");

                ComboxSeleccionarCodigoUser.Items.Clear();//la vacio 
                ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");//le añado mensaje
                LlenarComboxUser();//la vuelvo a llenar el combox de los id´s

                TextBoxIdusuario.Text = "";

                TextBoxNombreAdmon.Text = "";
                TextBoxDpiUsuarioAdmon.Text = "";
                TextBoxTelefonoAdmon.Text = "";
                TextBoxEmailAdmon.Text = "";
                TextBoxUserAdmon.Text = "";
                TextBoxPasswordAdmon.Text = "";
                RolUsurio = "";

                TextBoxIdusuario.Enabled = true;
                LabelRolModificar.Text = "Modificacion:";


            }
        }

        protected void EliminarUser_Click(object sender, EventArgs e)
        {

            try
            {

                conexion3.Open();
                MySqlCommand EliminarUSuario = new MySqlCommand("delete from usuario_administracion where id_usuario='" + TextBoxIdusuario.Text + "'", conexion3);
                EliminarUSuario.ExecuteNonQuery();
                conexion3.Close();
                Response.Write("Se A Eliminado Con Exito");
                ComboxSeleccionarCodigoUser.Items.Clear();//la vacio 
                ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");//le añado mensaje
                LlenarComboxUser();//la vuelvo a llenar el combox de los id´s

                TextBoxIdusuario.Text = "";

                TextBoxNombreAdmon.Text = "";
                TextBoxDpiUsuarioAdmon.Text = "";
                TextBoxTelefonoAdmon.Text = "";
                TextBoxEmailAdmon.Text = "";
                TextBoxUserAdmon.Text = "";
                TextBoxPasswordAdmon.Text = "";
                RolUsurio = "";

                TextBoxIdusuario.Enabled = true;
                LabelRolModificar.Text = "Modificacion:";

            }
            catch (Exception)
            {
                conexion3.Close();
                Response.Write("No Se A Eliminado");
                ComboxSeleccionarCodigoUser.Items.Clear();//la vacio 
                ComboxSeleccionarCodigoUser.Items.Add("Seleccionar_User");//le añado mensaje
                LlenarComboxUser();//la vuelvo a llenar el combox de los id´s

                TextBoxIdusuario.Text = "";

                TextBoxNombreAdmon.Text = "";
                TextBoxDpiUsuarioAdmon.Text = "";
                TextBoxTelefonoAdmon.Text = "";
                TextBoxEmailAdmon.Text = "";
                TextBoxUserAdmon.Text = "";
                TextBoxPasswordAdmon.Text = "";
                RolUsurio = "";

                TextBoxIdusuario.Enabled = true;
                LabelRolModificar.Text = "Modificacion:";

            }
        }
    }
}