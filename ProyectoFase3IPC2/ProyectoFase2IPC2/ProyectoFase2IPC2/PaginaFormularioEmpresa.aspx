﻿<%@ Page Language="C#" EnableEventValidation="true" AutoEventWireup="true" CodeBehind="PaginaFormularioEmpresa.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaFormularioEmpresa"   %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Formulario-Empresa-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoFormulario">
    <form id="form1" runat="server">
        
                                 
        <div class="OtroH11">
    <h1  class="OtroH11">INSCRIPCION AL SITIO WEB
        
            </h1>
           
            
&nbsp;
                

            <br />

        </div>

        <div>

        </div>
       
        <div style="margin-left: 40px">


            <asp:Label ID="LabelDPI" runat="server" Text="Nombre* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<asp:TextBox ID="TextBoxNombreEmpresa" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label1" runat="server" Text="Direccion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxDireccionEmpresa" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="EmailEmpresa" runat="server" Text="Email:" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxEmailEmpresa" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
            <div>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                <asp:Label ID="Label2" runat="server" Text="Region* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            
            <asp:DropDownList ID="ComboBoxRegionEmpresa" runat="server" Height="28px" Width="170px" AutoPostBack="True" OnSelectedIndexChanged="ComboBoxRegionEmpresa_SelectedIndexChanged">
                <asp:ListItem>Seleccione_Region:</asp:ListItem>
            </asp:DropDownList>
                                    <br />
                                    <asp:Label ID="Label9" runat="server" BackColor="White" Font-Bold="True" ForeColor="Red" Text="Tipo* :"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:DropDownList ID="ComboBoxTipoEmpresa" runat="server" AutoPostBack="True" Height="28px" OnSelectedIndexChanged="ComboBoxTipoEmpresa_SelectedIndexChanged" Width="173px">
                                        <asp:ListItem>Seleccione_Tipo:</asp:ListItem>
                                    </asp:DropDownList>
                                    </ContentTemplate>
                  <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboBoxRegionEmpresa"
                                EventName="SelectedIndexChanged" />
                                 </Triggers>
                  <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboBoxTipoEmpresa"
                                EventName="SelectedIndexChanged" />
                                 </Triggers>
                                        
                </asp:UpdatePanel>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            </div>
            <br />
             <asp:Label ID="Label3" runat="server" Text="Fotografia* :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            <asp:FileUpload ID="CargarFotos" runat="server" />
            &nbsp;<br />
            <br />
             <asp:Label ID="Label4" runat="server" Text="Telefono :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxTelefonoEmpresa" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="LabelHoraAtencionInicio" runat="server" Text="Hora_De_Atencion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxHoraEmpresaIni" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
             
            <br />
            <br />
                         <asp:Label ID="LabelHoraAtencionFin" runat="server" Text="Hora_Fin_Atencion :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxHoraFinEmpre" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>

            <br />
            <br />
                         <asp:Label ID="LabelTarifaMuseo" runat="server" Text="Tarifa :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxTarifaEmpresa" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
            <div>
                           <asp:Label ID="LabelEspecialidadRestaurante" runat="server" Text="Especialidad*  :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                            
            <asp:DropDownList ID="ComboBoxEspecialidadEmpresa" runat="server" Height="33px" Width="176px" AutoPostBack="True" OnSelectedIndexChanged="ComboBoxEspecialidadEmpresa_SelectedIndexChanged">
                <asp:ListItem>Seleccione_Especialidad</asp:ListItem>
            </asp:DropDownList>
                                    <br />
                                    <asp:Label ID="LabelServiciosHotel" runat="server" BackColor="White" Font-Bold="True" ForeColor="Red" Text="Servicios * :"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:DropDownList ID="ComboBoxServiciosEmpresa" runat="server" AutoPostBack="True" Height="32px" OnSelectedIndexChanged="ComboBoxServiciosEmpresa_SelectedIndexChanged" Width="175px">
                                        <asp:ListItem>Seleccione_Servicios:</asp:ListItem>
                                    </asp:DropDownList>
                                    </ContentTemplate>
                 <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboBoxEspecialidadEmpresa"
                                EventName="SelectedIndexChanged" />
<asp:AsyncPostBackTrigger ControlID="ComboBoxServiciosEmpresa" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                 </Triggers>

                <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboBoxServiciosEmpresa"
                                EventName="SelectedIndexChanged" />
                                 </Triggers>
                             
                             
                </asp:UpdatePanel>
                </div>
            <br />
            <br />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
            
            <br />
            <br />
         

        </div>


    <div>
 
        <br />
<br />
        <div class="OtroH11">
        <asp:Button ID="BotonCargarFormulario" runat="server" class="BotonesC" Text="CARGAR FORMULARIO" OnClick="BotonCargarFormulario_Click" />

        &nbsp;&nbsp; 
        &nbsp;&nbsp;
        &nbsp;<br />
        <br />

        <br />
            </div>
    </div>
       


    


    </form>
   
    

    

</body>

</html>
