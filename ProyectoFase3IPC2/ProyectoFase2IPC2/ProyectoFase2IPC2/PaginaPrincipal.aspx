﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaPrincipal.aspx.cs" Inherits="ProyectoFase2IPC2.WebForm1" %>

<!DOCTYPE HTML>
<!--
	Aerial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Turismo-Pagina-Principal</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
      

		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<form id="form1" runat="server">
		<div id="wrapper">
			<div id="bg"></div>
			<div id="overlay"></div>
			<div id="main">

				<!-- Header -->
					<header id="header">
						<h1>INSTITUTO DE TURISMO NACIONAL</h1>
                        <p>&nbsp;</p>
						
						
							
							
							    &nbsp;&nbsp;&nbsp;
						
						
							
							
							    <asp:Button ID="IniciarSesion" runat="server"  Text="Iniciar Sesion" OnClick=" IniciarSesion_Click" BackColor="Black"  Font-Bold="True" Font-Italic="True" ForeColor="Lime" Height="66px" Width="218px" Font-Overline="True" Font-Underline="True" Font-Size="XX-Large" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="SitiosEmpresass" runat="server" Text="Sitios/Empresas" BackColor="Black" Font-Bold="True" Font-Italic="True" ForeColor="Lime" Height="66px" Width="245px" OnClick="SitiosEmpresass_Click" Font-Overline="True" Font-Underline="True" Font-Size="XX-Large" />
						        &nbsp;<br />
                        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        &nbsp;<asp:Button ID="FormularioEmpresa" runat="server" Text="Formulario Empresas" BackColor="Black" Font-Bold="True" Font-Italic="True" ForeColor="Lime" Height="66px" Width="323px" Font-Overline="True" Font-Underline="True" Font-Size="XX-Large" OnClick="FormularioEmpresa_Click" />
						
						
					</header>

				<!-- Footer -->
					<footer id="footer">
						<span class="copyright">&copy;
					</footer>

			</div>
		</div>
		<script>
			window.onload = function() { document.body.classList.remove('is-preload'); }
			window.ontouchmove = function() { return false; }
			window.onorientationchange = function() { document.body.scrollTop = 0; }
		</script>
	    </form>
	</body>
</html>
