﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaAgente.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaAgente" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Agente-Page</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoAgente">
    <form id="form1" runat="server">
        <div class="OtroH11">
    <h1  class="OtroH11">MODULO AGENTE
            </h1>
            <div class="OtroH11">   
                        <asp:Button ID="BotonCerrarSesion" runat="server" class="Botones3C" Text="Cerrar Sesion" OnClick="BotonCerrarSesion_Click" />

            </div>
            <h1  class="OtroH11">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
&nbsp;<asp:DropDownList ID="ComboxSeleccionarCodigRegion" runat="server" Height="54px" Width="193px" style="margin-left: 0px" AutoPostBack="True" OnSelectedIndexChanged="ComboxSeleccionarCodigoUser_SelectedIndexChanged">
                    <asp:ListItem>Seleccionar_Region</asp:ListItem>
                    <asp:ListItem>1-Norte</asp:ListItem>
                    <asp:ListItem>2-Sur</asp:ListItem>
                    <asp:ListItem>3-Este</asp:ListItem>
                    <asp:ListItem>4-Oeste</asp:ListItem>
                </asp:DropDownList>
                                    </ContentTemplate>
                       <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboxSeleccionarCodigRegion"
                                EventName="SelectedIndexChanged" />
                                 </Triggers>
                      </asp:UpdatePanel>
&nbsp;<asp:DropDownList ID="ComboBoxTipoInscripcion" runat="server" Height="28px" Width="203px" OnSelectedIndexChanged="ComboBoxTipoInscripcion_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem>Tipo Inscripcion</asp:ListItem>
                    <asp:ListItem>SitioTuristico</asp:ListItem>
                    <asp:ListItem>EmpresaTuristica</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;<asp:DropDownList ID="ComboxTecnico" runat="server" Height="54px" Width="193px" style="margin-left: 0px" AutoPostBack="True" OnSelectedIndexChanged="ComboxTecnico_SelectedIndexChanged">
                    <asp:ListItem>Seleccionar Tecnico</asp:ListItem>
                </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="BuscarFormulariosParaInscripcion" runat="server" class="BotonesC" Text="OBTENER FORMULARIOS" OnClick="BuscarFormulariosParaInscripcion_Click" />
            </h1>

            <br />

            <asp:GridView ID="TablaFormularios" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="TablaFormularios_SelectedIndexChanged">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>

        </div>


    <div class="OtroH11">
 
        <br />
        <br />
        <br />
        

        
        <asp:Button ID="Cargar_Sitio_Empresa" runat="server" class="BotonesC" Text="CARGAR" OnClick="Cargar_Sitio_Empresa_Click" />
        <br />
        <br />

        <br />

    </div>
       


    


    </form>
   
    

    

</body>

</html>
