﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFase2IPC2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void IniciarSesion_Click(object sender, EventArgs e)
        {
            Response.Redirect("Loguin.aspx");
        }

        protected void SitiosEmpresass_Click(object sender, EventArgs e)
        {
            string _open = "window.open('PaginaVisitante.aspx', '_newtab');";//sirve para abrir en una nueva pestaña otra ventana o web form
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

       
        protected void FormularioEmpresa_Click(object sender, EventArgs e)
        {
            PaginaFormularioEmpresa.SoloCuandoAccion = 'S';
            string _open = "window.open('PaginaFormularioEmpresa.aspx', '_newtab');";//sirve para abrir en una nueva pestaña otra ventana o web form
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        
    }
}