﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaAdministrador.aspx.cs" Inherits="ProyectoFase2IPC2.PaginaAdministrador" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>AdministradorPage</title>
    <!-- meta tags -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Art Sign Up Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, 
		Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <!-- /meta tags -->
    <!-- custom style sheet -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- /custom style sheet -->
    <!-- fontawesome css -->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <!-- /fontawesome css -->
    <!-- google fonts-->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- /google fonts-->

</head>


<body class="FondoAdmin">
    <form id="form1" runat="server">
        <div class="OtroH11">
    <h1  class="OtroH11">MODULO ADMINNISTRADOR
        
            </h1>
            <div class="OtroH11">
                <asp:Button ID="ButtonCerrarSesionAdmon" class="Botones3C" runat="server" Text="Cerrar Sesion" OnClick="ButtonCerrarSesionAdmon_Click" />
            </div>
            <h1  class="OtroH11">
                <asp:DropDownList ID="ComboxSeleccionarCodigoUser" runat="server" Height="54px" Width="193px" style="margin-left: 0px">
                    <asp:ListItem>Seleccionar_User</asp:ListItem>
                </asp:DropDownList>
&nbsp;
                <asp:Button ID="BuscarIDUser" runat="server" class="BotonesC" Text="BUSCAR" OnClick="BuscarIDUser_Click" />
            </h1>

        </div>

        <div>

        </div>
       
        <div style="margin-left: 40px">


            <br />


            <asp:Label ID="LabeIdUserAdmon" runat="server" Text="ID_USUARIO:" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxIdusuario" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />


            <asp:Label ID="LabelDPI" runat="server" Text="DPI :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <asp:TextBox ID="TextBoxDpiUsuarioAdmon" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label1" runat="server" Text="NOMBRE :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxNombreAdmon" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label2" runat="server" Text="EMAIL :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxEmailAdmon" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label3" runat="server" Text="TELEFONO :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxTelefonoAdmon" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label4" runat="server" Text="USUARIO :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxUserAdmon" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>
            <br />
            <br />
             <asp:Label ID="Label5" runat="server" Text="CONTRASEÑA :" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBoxPasswordAdmon" runat="server" BorderStyle="Solid" Width="224px" BackColor="White" BorderColor="Black"></asp:TextBox>

            <br />
            <br />
             <asp:Label ID="LabelRolUser" runat="server" Text="ROL USUARIO:" Font-Bold="True" ForeColor="Red" BackColor="White"></asp:Label>
            <br />
            <br />
             <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            
            <asp:DropDownList ID="ComboBoxRoles" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>Seleccione-Rol:</asp:ListItem>
                <asp:ListItem>agente</asp:ListItem>
                <asp:ListItem>tecnico</asp:ListItem>
            </asp:DropDownList>
                                    <br />
                                    <br />
                                    <asp:Label ID="LabelRolModificar" runat="server" BackColor="White" Font-Bold="True" ForeColor="Red" Text="Modificacion:"></asp:Label>
                                    </ContentTemplate>
                  <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="ComboBoxRoles"
                                EventName="SelectedIndexChanged" />
                                 </Triggers>
                </asp:UpdatePanel>

        </div>


    <div>
 
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <div class="OtroH11">
        <asp:Button ID="CrearUser" runat="server" class="BotonesC" Text="Crear Usuario" OnClick="CrearUser_Click" />

        &nbsp;&nbsp; <asp:Button ID="ModificarUser" runat="server" class="BotonesC" Text="Modificar Usuario" OnClick="ModificarUser_Click" />
        &nbsp;&nbsp;
        &nbsp;<asp:Button ID="EliminarUser" runat="server" class="BotonesC" Text="Eliminar Usuario" OnClick="EliminarUser_Click" />

        <br />
        <br />

        <br />
            </div>
    </div>
       


    


    </form>
   
    

    

</body>

</html>